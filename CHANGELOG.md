## v2.0.0
- new file format version 4 -> same structure as the native files
- compressed mesh support
- compound file (aka shrine/field) support
- model importer
- new file parser
- bugfix in class header, offset is always now correct
- completely refactored code
- better test coverage

## v1.3.0
- new shape type: sphere

## v1.2.0
- added climbable flag to entries