/**
* @copyright 2019 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

module.exports = class BvTree
{
    constructor(domain, childA, childB)
    {
        this.domain = domain;
        this.childA = childA;
        this.childB = childB;
    }
};