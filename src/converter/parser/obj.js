/**
* @copyright 2019 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

// @TODO write more tests for the split up
const ELEMENT_LIMIT = 126;

function getPolygonMin(poly)
{
    return [
        poly.reduce((min, vertex) => Math.min(vertex[0], min), Infinity),
        poly.reduce((min, vertex) => Math.min(vertex[1], min), Infinity),
        poly.reduce((min, vertex) => Math.min(vertex[2], min), Infinity),
    ];
}

function sortPolygons(a, b) 
{
    const minA = getPolygonMin(a);
    const minB = getPolygonMin(b);
    const diff = [0,0,0];
    for(let i=0; i<3; ++i)
    {
        diff[i] = (Math.round((minA[i] - minB[i])));
        /*if(Math.abs(diff[i]) < 10) {
            diff[i] = 0;
        }*/
    }
    return diff[0] || diff[2] || diff[1];
}

function normalizeObject(obj)
{
    // map the indices + vertices into an big vertex array without indices
    const polygonArray = [];
    for(let pair of obj.indices) {
        polygonArray.push(pair.map(i => obj.vertices[i]));
    }

    const polygonArraySort = polygonArray.sort(sortPolygons);

    const normObjects = [];
    let vertexMap = new Map(); 
    let objectIndex = -1;    
    let currentIndex = -1;

    const addObject = () => 
    {
        currentIndex = 0;
        vertexMap = new Map();
        ++objectIndex;

        normObjects.push({
            name: obj.name,
            vertices: [], 
            indices: []
        });
    };

    for(let polygon of polygonArraySort)
    {
        if(currentIndex == -1) {
            addObject();
        }

        const currentObj = normObjects[objectIndex];
        const indexPair = polygon.map(vertex => 
        {
            const vertexHash = vertex.join("|");
            if(vertexMap.has(vertexHash)) {
                return vertexMap.get(vertexHash);
            }

            vertexMap.set(vertexHash, currentIndex);
            currentObj.vertices.push(vertex);
            return currentIndex++;
        });

        currentObj.indices.push(indexPair);

        if((currentIndex >= ELEMENT_LIMIT-3) || 
            currentObj.indices.length >= ELEMENT_LIMIT || 
            currentObj.vertices.length >= ELEMENT_LIMIT) 
        {
            currentIndex = -1;
        }
    }
    
    return normObjects;
}

module.exports = async (fileStream) => 
{
    let currentObject = undefined;
    const objData = {objects: []};

    let indexOffset = 1;

    fileStream.on('line', line => 
    {
        const parts = line.trim().split(" ");

        switch(parts[0])
        {
            case 'o':
                
                if(currentObject) {
                    indexOffset += currentObject.vertices.length;
                    objData.objects.push(...normalizeObject(currentObject));
                }

                currentObject = {
                    name: parts[1],
                    vertices: [],
                    indices: []
                };
                
            break;

            case 'v':
                currentObject.vertices.push(
                    parts.slice(1).map(x => parseFloat(x))
                );
            break;

            case 'f':
                // @TODO proper support for polygons
                const indices = parts.slice(1, 5).map(x => parseInt(x.split("/")) - indexOffset);
                if(indices.length > 3) {
                    console.warn("Warning: detected a polygon with more than 3 indices, this may cause errors, please triangulate the file!");
                }

                if(indices.find(i => i >= currentObject.vertices.length)) {
                    throw "Object contains indices bigger than the vertex array";
                }

                currentObject.indices.push(indices);
            break;
        }
    });

    return new Promise((resolve, reject) => {
        fileStream.on('close', () => {
            objData.objects.push(...normalizeObject(currentObject));
            resolve(objData);
        });
    });
}