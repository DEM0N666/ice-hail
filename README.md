# **Ice-Hail**

![Ice-Hail](https://ice-hail.gitlab.io/logo.png)

# Overview

Ice-Hail is a simple CLI tool to create valid Havok Collision data from a JSON definition file.<br/>
This format is used to define collision for objects, shrines and overworld maps.

An older GUI version of this tool can be found at: https://ice-hail.gitlab.io

# Installation

Install it globally with yarn or npm with: <br/>
`npm install -g ice-hail`

> **Note** <br/>
> You'll need at least [NodeJS 11.6](https://nodejs.org/en/) to use this.

# Usage

Ice-Hail comes with three commands: `ice-hail-create`, `ice-hail-import` and `ice-hail-parse`. <br/>
Each command can be called without params or with `-h` to get a help printed out.

If you are only interested in creating custom shrines, take a look at the Examples section of this readme.

## Create

`ice-hail-create` is used to create Havok files from a JSON definition. <br/>
This will take a JSON as an input and creates a hkrb / hksc collision file.

A few example files can be found in the repo's integration tests at:
[Test-Files](https://gitlab.com/ice-hail/ice-hail/tree/master/test/integration/files)

Example usage:
```sh
ice-hail-create test.json
```
```sh
ice-hail-create another-test.json output.hkrb
```

## Import

`ice-hail-import` is used to import 3D-Model files. <br/>
This will create a JSON file which can then be used to create a havok file. <br/>
The reason for creating a JSON is that it can be manually edited before creating a binary file.

Currently it only supports compound shapes (aka shrines and field).<br/>
An import for normal collision (e.g. actors) will be added later.

Example usage:
```sh
ice-hail-import -c testModel.obj
```

>**Note** <br/>
>You can add the `-p` flag to create a beautified JSON.

## Parse [WIP]

The last command, `ice-hail-parse`, is used to create a JSON file from a binary havok file. <br/>
This can be used to read out any collision file in the game to some degree. <br/>
While it can be used to read, edit and then re-create the model again, the reader is still work-in-progress.<br/>
So in most cases, re-creating the model will not result in an identical file.

Example usage:
```sh
ice-hail-parse TwnObj_SomeNiceFile.hkrb
```

>**Note** <br/>
>Be sure that the file is not yaz0 encoded.

# Examples

## Shrines

To create collision for custom shrines, you first need a 3D-Model of the collision as an .OBJ file, let's call it `test.obj`.

First, run the import and create command:
```sh
ice-hail-import -c test.obj
ice-hail-create test.json
```
Then rename and yaz0 encode the output `test.hksc` file to match the shrine file, which can be found in `Physics\StaticCompound\CDungeon` in the dungeon pack.

# Links and Stuff

Link to the BotW modding discord:
https://discord.gg/NzHApcX

For Questions, ask me there (@HailToDodongo).

<br/>

## Disclaimer
This tool was created by reverse-engineering the game and does NOT contain any game files itself.<br/>
Please also respect hard working game developers and the law. <br/>
Only use ice-hail with your own games / legally created backups of your games, thank you!