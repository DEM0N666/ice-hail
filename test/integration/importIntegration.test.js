const fs = require("fs-extra");
const convertFile = require('../../src/converter/convert');

describe('Import Integration-Test', () => {

    const testFiles = [
        {
            name: "Compound Files",
            files: {
                "multi-obj-norm": ['Multiple Objects, normalized OBJ file', {compound: true}],
                "multi-obj-quads": ['Multiple Objects, OBJ file with quads', {compound: true}],
                "multi-obj-uv": ['Multiple Objects, OBJ file with UVs set', {compound: true}],
                "multi-obj-norm-uv-edge": ['Multiple Objects, UVs/edges/normals', {compound: true}],
            }
        }
    ];
    
    describe("Import files", () => {
        testFiles.forEach(testCase => 
        {
            describe(testCase.name, () => {
                for(let file in testCase.files) 
                {
                    test(testCase.files[file][0], async () => {
                        const createdJson = await convertFile("test/integration/files/" + file + ".obj", testCase.files[file][1]);
                        const testJson = await fs.readJSON("test/integration/files/" + file + ".json");
                        expect(createdJson).toMatchCloseTo(testJson, 6);
                    });   
                }
            });    
        });
    });
});