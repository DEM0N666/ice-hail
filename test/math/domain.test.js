/**
* @copyright 2019 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

const {getDomainFromParts, mergeDomains} = require('../../src/math/domain');

describe("math", () => 
{
    describe("getDomainFromParts", () => 
    {
        test('single entry, single vertex', () => {
            const parts = [
                {
                    vertices: [[1.0, 2.0, -3.0]]
                }
            ];
            expect(getDomainFromParts(parts, 0.0, 0.0)).toEqual({
                min: [1.0, 2.0, -3.0],
                max: [1.0, 2.0, -3.0],
            });
        });

        test('single entry, multiple vertices', () => {
            const parts = [
                {
                    vertices: [
                        [ 1.0, 2.0, -3.0],
                        [ 2.0, 5.0,  9.0],
                        [-1.0, 0.0, -4.0],
                    ]
                }
            ];
            expect(getDomainFromParts(parts, 0.0, 0.0)).toEqual({
                min: [-1.0, 0.0, -4.0],
                max: [ 2.0, 5.0,  9.0],
            });
        });

        test('single entry, multiple vertices with zero size component', () => {
            const parts = [
                {
                    vertices: [
                        [ 1.0, 2.0, -3.0],
                        [ 2.0, 2.0,  9.0],
                        [-1.0, 2.0, -4.0],
                    ]
                }
            ];
            // Y is constant here, e.g. floor collision
            expect(getDomainFromParts(parts, 0.0, 0.0)).toEqual({
                min: [-1.0, 2.0, -4.0],
                max: [ 2.0, 2.0,  9.0],
            });
        });

        test('multiple entries, multiple vertices', () => {
            const parts = [
                {
                    vertices: [
                        [ 2.0, 5.0,  9.0],
                        [-1.0, 0.0, -4.0],
                    ]
                },
                {
                    vertices: [
                        [-7.0,  5.0,  3.0],
                        [ 1.0, -5.0,  0.0],
                    ]
                }
            ];
            expect(getDomainFromParts(parts, 0.0, 0.0)).toEqual({
                min: [-7.0, -5.0, -4.0],
                max: [ 2.0,  5.0,  9.0],
            });
        });
    });

    describe("mergeDomains", () => 
    {
        test('should use the smallest min and biggest max', () => {

            const mergedDomain = mergeDomains({
                min: [-1.0, -5.0, 0.0],
                max: [ 3.0,  6.0, 8.0],
            },{
                min: [-7.0, -5.0, 5.0],
                max: [ 2.0,  5.0, 9.0],
            });

            expect(mergedDomain).toEqual({
                min: [-7.0, -5.0, 0.0],
                max: [ 3.0, 6.0,  9.0],
            });
        });
    });
});