/**
* @copyright 2019 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

const Vector3 = require("../../src/math/vector3");
const {pointsToPlane} = require("../../src/math/plane");

describe('pointsToPlane', () => 
{
    test('points in a line should fail', () => {
        expect(() => {
            pointsToPlane(
                new Vector3([1.0, 2.0, 3.0]),
                new Vector3([1.0, 3.0, 3.0]),
                new Vector3([1.0, 4.0, 3.0])
            )
        }).toThrow();
    });

    test('simple axis aligned plane', () => {
        expect(pointsToPlane(
            new Vector3([1.0, 2.0, 3.0]),
            new Vector3([-1.0, 2.0, 5.0]),
            new Vector3([3.0, 2.0, 0.0])
        )).toEqual([-0.0, -1.0, -0.0,  2.0]);
    });

    test('rotated plane', () => {
        const plane = pointsToPlane(
            new Vector3([3.0, 0.0, 0.0]),
            new Vector3([2.0, 0.5, 0.0]),
            new Vector3([2.0, 0.0, 1.0])
        );
        
        expect(plane[0]).toBeCloseTo(0.4082, 4);
        expect(plane[1]).toBeCloseTo(0.8165, 4);
        expect(plane[2]).toBeCloseTo(0.4082, 4);
        expect(plane[3]).toBeCloseTo(-1.2247, 4);
    });
});