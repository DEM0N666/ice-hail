const Quaternion = require("../../src/math/quaternion");

describe('Quaternion', () => 
{
    test('no rotation', () => 
    {   
        expect(
            Quaternion.fromEuler(0.0, 0.0, 0.0)
        ).toEqual([1.0, 0.0, 0.0, 0.0]);
    });

    test('conversion + XYZ order', () => 
    {   
        const q = Quaternion.fromEuler(20.0, 30.0, 40.0);
        expect(q[0]).toBeCloseTo(0.909);
        expect(q[1]).toBeCloseTo(0.283);
        expect(q[2]).toBeCloseTo(0.297);
        expect(q[3]).toBeCloseTo(0.070);
    });

    test('over 180/360 degrees', () => 
    {   
        const q = Quaternion.fromEuler(500.0, 0.0, 0.0);
        expect(q[0]).toBeCloseTo(-0.342);
        expect(q[1]).toBeCloseTo(0.0);
        expect(q[2]).toBeCloseTo(0.0);
        expect(q[3]).toBeCloseTo(-0.94);
    });
});
