/**
* @copyright 2019 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

const PhysicsData = require("../../src/classes/hkpPhysicsData");
const BinaryFile = require("../../src/binaryFile");

describe(PhysicsData.name, () => 
{
    test('pointer', () => {
        const file = new BinaryFile();
        const entry = new PhysicsData(file, {
            children: []
        });
        entry._create();

        expect(entry.pointer).toEqual([0x0C, 0x20]);
    });

    test('integration test', () => {
        const file = new BinaryFile();
        const entry = new PhysicsData(file, {
            children: [{}]
        });
        entry._create();

        expect(file.getBuffer()).toEqualBinary(
`00000000: 0000 0000 0000 0000 0000 0000 0000 0000  ................
00000010: 0000 0001 8000 0001 0000 0000 0000 0000  ................
00000020: 0000 0000 0000 0000 0000 0000 0000 0000  ................`
        );
    });
});