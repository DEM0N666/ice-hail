/**
* @copyright 2019 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

const SphereShape = require("../../src/classes/hkpSphereShape");
const BinaryFile = require("../../src/binaryFile");

describe(SphereShape.name, () => 
{
    test('pointer', () => {
        const file = new BinaryFile();
        const entry = new SphereShape(file, {
            radius: 2.0
        });
        entry._create();

        expect(entry.pointer.length).toEqual(0);
    });

    test('integration test', () => {
        const file = new BinaryFile();
        const entry = new SphereShape(file, {
            radius: 2.0
        });
        entry._create();

        expect(file.getBuffer()).toEqualBinary(
`00000000: 0000 0000 0000 0000 0004 0000 1000 8002  ................
00000010: 4000 0000 0000 0000 0000 0000 0000 0000  @...............`
        );
    });

    test('integration test, no radius', () => {
        const file = new BinaryFile();
        const entry = new SphereShape(file, {});
        entry._create();

        expect(file.getBuffer()).toEqualBinary(
`00000000: 0000 0000 0000 0000 0004 0000 1000 8002  ................
00000010: 0000 0000 0000 0000 0000 0000 0000 0000  ................`
        );
    });
});