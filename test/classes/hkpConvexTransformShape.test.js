/**
* @copyright 2019 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

const ConvexTransformShape = require("../../src/classes/hkpConvexTransformShape");
const BinaryFile = require("../../src/binaryFile");

describe(ConvexTransformShape.name, () => 
{
    test('pointer', () => {
        const file = new BinaryFile();
        const entry = new ConvexTransformShape(file, {
            translate: [3.0, 4.0, 5.0],
        });
        entry._create();

        expect(entry.pointer.length).toEqual(0);
    });

    test('integration test, translate', () => {
        const file = new BinaryFile();
        const entry = new ConvexTransformShape(file, {
            translate: [3.0, 4.0, 5.0],
        });
        entry._create();

        expect(file.getBuffer()).toEqualBinary(
`00000000: 0000 0000 0000 0000 0004 0000 1000 8002  ................
00000010: 3d4c cccd 0000 0000 0000 0000 0000 0000  =LÌÍ............
00000020: 4040 0000 4080 0000 40a0 0000 0000 0000  @@..@...@.......
00000030: 3f80 0000 0000 0000 0000 0000 0000 0000  ?...............
00000040: 3f80 0000 3f80 0000 3f80 0000 3f80 0000  ?...?...?...?...`
        );
    });

    test('integration test, translate + scale', () => {
        const file = new BinaryFile();
        const entry = new ConvexTransformShape(file, {
            translate: [3.0, 4.0, 5.0],
            scale: [1.0, 2.0, 3.0],
        });
        entry._create();

        expect(file.getBuffer()).toEqualBinary(
`00000000: 0000 0000 0000 0000 0004 0000 1000 8002  ................
00000010: 3d4c cccd 0000 0000 0000 0000 0000 0000  =LÌÍ............
00000020: 4040 0000 4080 0000 40a0 0000 0000 0000  @@..@...@.......
00000030: 3f80 0000 0000 0000 0000 0000 0000 0000  ?...............
00000040: 3f80 0000 4000 0000 4040 0000 3f80 0000  ?...@...@@..?...`
        );
    });

    test('integration test, rotation', () => {
        const file = new BinaryFile();
        const entry = new ConvexTransformShape(file, {
            rotate: [45.0, 90.0, 180.0],
        });
        entry._create();

        expect(file.getBuffer()).toEqualBinary(
`00000000: 0000 0000 0000 0000 0004 0000 1000 8002  ................
00000010: 3d4c cccd 0000 0000 0000 0000 0000 0000  =LÌÍ............
00000020: 0000 0000 0000 0000 0000 0000 0000 0000  ................
00000030: 3e8a 8bd4 3f27 3d75 3e8a 8bd4 bf27 3d75  >..Ô?'=u>..Ô¿'=u
00000040: 3f80 0000 3f80 0000 3f80 0000 3f80 0000  ?...?...?...?...`
        );
    });

});