/**
* @copyright 2019 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

const RootLevelContainer = require("../../src/classes/hkRootLevelContainer");
const BinaryFile = require("../../src/binaryFile");

describe(RootLevelContainer.name, () => 
{
    test('pointer', () => {
        const file = new BinaryFile();
        const entry = new RootLevelContainer(file);
        entry._create();

        expect(entry.pointer).toEqual([
            0x00, 0x10, 0x10, 0x1C, 0x14, 0x30
        ]);
    });

    test('integration test', () => {
        const file = new BinaryFile();
        const entry = new RootLevelContainer(file);
        entry._create();

        expect(file.getBuffer()).toEqualBinary(
`00000000: 0000 0000 0000 0001 8000 0001 0000 0000  ................
00000010: 0000 0000 0000 0000 0000 0000 5068 7973  ............Phys
00000020: 6963 7320 4461 7461 0000 0000 0000 0000  ics Data........
00000030: 686b 7050 6879 7369 6373 4461 7461 0000  hkpPhysicsData..`
        );
    });
});