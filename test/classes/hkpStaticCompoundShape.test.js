/**
* @copyright 2019 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

const StaticCompoundShape = require("../../src/classes/hkpStaticCompoundShape");
const BinaryFile = require("../../src/binaryFile");

const collDataActors = {
    children: [
        {
            type: "hkpBoxShape",
            sizeHalf: [1,1,1],
            actorId: 42
        },
        {
            type: "hkpRandomOtherShape",
            sizeHalf: [2,2,2],
            actorId: 43
        }
    ]
};


describe(StaticCompoundShape.name, () => 
{
    test('pointer', () => {
        const file = new BinaryFile();
        const entry = new StaticCompoundShape(file, collDataActors);
        entry._create();

        expect(entry.pointer).toEqual([
            0x20, 0x70, 0x40, 0xF0
        ]);
    });

    test('integration test', () => {
        const file = new BinaryFile();
        const entry = new StaticCompoundShape(file, collDataActors);
        entry._create();

        expect(file.getBuffer()).toEqualBinary(
`00000000: 0000 0000 0000 0000 0004 0000 0000 0000  ................
00000010: 0200 0000 0000 0000 0800 0000 0000 0000  ................
00000020: 0000 0000 0000 0002 8000 0002 0000 0000  ................
00000030: 0000 0000 8000 0000 0000 0000 0000 0000  ................
00000040: 0000 0000 0000 0003 8000 0003 0000 0000  ................
00000050: c000 0000 c000 0000 c000 0000 0000 0000  À...À...À.......
00000060: 4000 0000 4000 0000 4000 0000 0000 0000  @...@...@.......
00000070: 0000 0000 0000 0000 0000 0000 3f00 0002  ............?...
00000080: 0000 0000 0000 0000 0000 0000 3f80 0000  ............?...
00000090: 3f80 0000 3f80 0000 3f80 0000 3f00 0000  ?...?...?...?...
000000a0: 0000 0000 9001 8000 0000 0000 0000 002a  ...............*
000000b0: 0000 0000 0000 0000 0000 0000 3f00 0002  ............?...
000000c0: 0000 0000 0000 0000 0000 0000 3f80 0000  ............?...
000000d0: 3f80 0000 3f80 0000 3f80 0000 3f00 0000  ?...?...?...?...
000000e0: 0000 0000 0000 0000 ffff ffff 0000 002b  ........ÿÿÿÿ...+
000000f0: 0000 0080 0001 7777 7700 0000 0000 0000  ......www.......
00000100: 0001 0000 0000 0000 0000 0000 0000 0000  ................`
        );
    });
});