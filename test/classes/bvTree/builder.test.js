/**
* @copyright 2019 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

const {Domain, DomainEntry} = require('../../../src/math/domain');
const {buildTree, _getMinDomainEntries} = require('../../../src/classes/bvTree/builder');
const BvTree = require('../../../src/classes/bvTree/tree');

/**
 * Prints a tree, used for debugging
 * @param {BvTree} tree 
 * @param {String} level 
 */
function treeToString(tree, level = "")
{
    let str = level + `[${tree.domain.min.join(", ")}] -> [${tree.domain.max.join(", ")}]`;
    if(tree.index >= 0) {
        str += " @ " + tree.index;
    }
    str += "\n";

    if(tree.childA && tree.childB) {
        str += treeToString(tree.childA, level + "    ");
        str += treeToString(tree.childB, level + "    ");
    }
    return str;
}

describe("bvTree.builder", () => 
{
    describe("buildTree", () => {

        test('empty entry-array', () => {
            expect(buildTree(null)).toBeUndefined();
            expect(buildTree([])).toBeUndefined();
        });

        test('test tree, n=1', () => {
            const tree = buildTree([
                new DomainEntry(new Domain([1,2,3], [4,5,6]), 5)
            ]);
            expect(tree).toEqual(
                new DomainEntry(new Domain([1,2,3], [4,5,6]), 5),
            );
        });

        test('test tree, n=3', () => {
            const tree = buildTree([
                new DomainEntry(new Domain([1,1,1], [3,2,3]), 5),
                new DomainEntry(new Domain([3,3,3], [4,4,4]), 6),
                new DomainEntry(new Domain([2,2,2], [3,3,3]), 7) 
            ]);
            expect(tree).toEqual(
                new BvTree(
                    new Domain([1, 1, 1], [4, 4, 4]),
                    new DomainEntry(new Domain([3,3,3], [4,4,4]), 6),
                    new BvTree(
                        new Domain([1, 1, 1], [3, 3, 3]),
                        new DomainEntry(new Domain([1,1,1], [3,2,3]), 5),
                        new DomainEntry(new Domain([2,2,2], [3,3,3]), 7),
                    )
            ));
        });

        test('test tree, n=5', () => {
            const tree = buildTree([
                new DomainEntry(new Domain([ 1,  1,  1], [ 3,  3,  3]), 2),
                new DomainEntry(new Domain([-1, -1, -1], [-2, -2, -2]), 3), // A
                new DomainEntry(new Domain([-2,  2,  2], [ 3,  3,  3]), 1),
                new DomainEntry(new Domain([-1, -1, -2], [ 1,  0,  2]), 5), // B
                new DomainEntry(new Domain([ 2,  3,  4], [ 5,  6,  5]), 9)
            ]);

            expect(tree).toEqual(
                new BvTree(
                    new Domain([-2, -1, -2], [5, 6, 5]),
                    new DomainEntry(new Domain([ 2,  3,  4], [ 5,  6,  5]), 9),
                    new BvTree(
                        new Domain([-2, -1, -2], [3, 3, 3]),
                        new BvTree(
                            new Domain([-1, -1, -2], [1, 0, 2]),
                            new DomainEntry(new Domain([-1, -1, -1], [-2, -2, -2]), 3),
                            new DomainEntry(new Domain([-1, -1, -2], [ 1,  0,  2]), 5)
                        ),
                        new BvTree(
                            new Domain([-2, 1, 1], [3, 3, 3]),
                            new DomainEntry(new Domain([ 1,  1,  1], [ 3,  3,  3]), 2),
                            new DomainEntry(new Domain([-2,  2,  2], [ 3,  3,  3]), 1)
                        )
                    )
            ));
        });
    });

    describe("_getMinDomainEntries", () => {

        test('empty entry-array', () => {
            expect(_getMinDomainEntries([])).toBeUndefined();
        });

        test('single entry-array', () => {
            expect(_getMinDomainEntries([
                new DomainEntry(new Domain([1,2,3], [4,5,6]), 42)
            ])).toEqual(
                [0, 0, new Domain([1,2,3], [4,5,6])]
            );
        });

        test('min test, n=3', () => {
            expect(_getMinDomainEntries([
                new DomainEntry(new Domain([1,1,1], [3,2,3]), 5), // A
                new DomainEntry(new Domain([3,3,3], [4,4,4]), 6),
                new DomainEntry(new Domain([2,2,2], [3,3,3]), 7)  // B
            ])).toEqual([
                0, 2, new Domain([1,1,1], [3,3,3])
            ]);
        });

        test('min test, n=5', () => {
            expect(_getMinDomainEntries([
                new DomainEntry(new Domain([ 1,  1,  1], [ 3,  3,  3]), 2),
                new DomainEntry(new Domain([-1, -1, -1], [-2, -2, -2]), 3), // A
                new DomainEntry(new Domain([-2,  2,  2], [ 3,  3,  3]), 1),
                new DomainEntry(new Domain([-1, -1, -2], [ 1,  0,  2]), 5), // B
                new DomainEntry(new Domain([ 2,  3,  4], [ 5,  6,  5]), 9)
            ])).toEqual([
                1, 3, new Domain([-1, -1, -2], [1, 0, 2])
            ]);
        });
    });
});  
