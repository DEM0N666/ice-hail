/**
* @copyright 2019 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

const StaticCompoundInfo = require("../../src/classes/StaticCompoundInfo");
const BinaryFile = require("../../src/binaryFile");

const collDataActors = {
    actors: [{
        id: 0,
        hashId: "01234567",
        strHash: "ABCDEFAB",
    },
    {
        id: 2,
        hashId: "00112233",
        strHash: "B00B5111",
    }]
};

const collDataEmpty = {
    actors: []
}; 

describe(StaticCompoundInfo.name, () => 
{
    test('pointer, with actors', () => {
        const file = new BinaryFile();
        const entry = new StaticCompoundInfo(file, collDataActors);
        entry._create();

        expect(entry.pointer).toEqual([
            0x04, 0x20, 0x10, 0x40
        ]);
    });

    test('pointer, no actors', () => {
        const file = new BinaryFile();
        const entry = new StaticCompoundInfo(file, collDataEmpty);
        entry._create();

        expect(entry.pointer.length).toEqual(0);
    });

    test('integration test, with actors', () => {
        const file = new BinaryFile();
        const entry = new StaticCompoundInfo(file, collDataActors);
        entry._create();
        entry.setHkrbOffset(0x00112233);

        expect(file.getBuffer()).toEqualBinary(
`00000000: 0011 2233 0000 0000 0000 0002 8000 0002  .."3............
00000010: 0000 0000 0000 0002 8000 0002 0000 0000  ................
00000020: 0123 4567 abcd efab 0000 0000 0000 0000  .#Eg«Íï«........
00000030: 0011 2233 b00b 5111 0000 0002 0000 0002  .."3°.Q.........
00000040: 0000 0000 0000 0000 0000 0000 0000 0002  ................
00000050: 0000 0002 0000 0000 0000 0000 0000 0000  ................`
        );
    });

    test('integration test, no actors', () => {
        const file = new BinaryFile();
        const entry = new StaticCompoundInfo(file, collDataEmpty);
        entry._create();
        entry.setHkrbOffset(0x00112233);

        expect(file.getBuffer()).toEqualBinary(
`00000000: 0011 2233 0000 0000 0000 0000 8000 0000  .."3............
00000010: 0000 0000 0000 0000 8000 0000 0000 0000  ................`
        );
    });
});