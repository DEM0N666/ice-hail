/**
* @copyright 2019 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

const {readIndices, writeIndices} = require('../../../src/classes/compressedMesh/indices');
const BinaryFile = require('../../../src/binaryFile');

describe("compressedMesh", () => 
{
    describe("readIndices", () => 
    {
        test('indices, quads', () => {
            const testfile = new BinaryFile(Buffer.from([
                0, 1, 2, 3,
                3, 4, 5, 6,
            ]));

            const indices = readIndices(testfile, 2);
            expect(indices).toEqual([
                [0, 1, 2, 3],
                [3, 4, 5, 6],
            ]);
        });

        test('indices with triangles, 4th value is cut ', () => {
            const testfile = new BinaryFile(Buffer.from([
                0, 1, 2, 2,
                128, 29, 140, 14,
                2, 3, 4, 4,
            ]));

            const indices = readIndices(testfile, 3);
            expect(indices).toEqual([
                [0, 1, 2],
                [128, 29, 140, 14],
                [2, 3, 4],
            ]);
        });
    });

    describe("writeIndices", () => 
    {
        test('indices, quads', () => {
            const testfile = new BinaryFile();
            writeIndices(testfile, [
                [0, 1, 2, 3],
                [3, 4, 5, 6],
                [3, 4, 5, 0],
            ]);

            expect(testfile.getBuffer()).toEqual(
                Buffer.from([
                    0, 1, 2, 3,
                    3, 4, 5, 6,
                    3, 4, 5, 0,
                ])
            );
        });

        test('indices with triangles, 4th value is repeated ', () => {
            const testfile = new BinaryFile();
            writeIndices(testfile, [
                [0, 1, 2],
                [3, 4, 5, 6],
                [100, 123, 20],
            ]);

            expect(testfile.getBuffer()).toEqual(
                Buffer.from([
                    0, 1, 2, 2,
                    3, 4, 5, 6,
                    100, 123, 20, 20,
                ])
            );
        });
    });
});