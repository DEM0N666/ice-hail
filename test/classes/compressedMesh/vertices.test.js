/**
* @copyright 2019 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

const {readVertices, readSharedVertices, writeVertices, writeSharedVertices, getVertexScale} = require('../../../src/classes/compressedMesh/vertices');
const Vector3 = require('../../../src/math/vector3');
const BinaryFile = require('../../../src/binaryFile');

const defaultOffset = [0,0,0];
const defaultScale = getVertexScale({
    min: [0,0,0], max: [1,1,1]
});
const MAX_X = 0b11111111111;
const MAX_Y = 0b11111111111;
const MAX_Z =  0b1111111111;

describe("compressedMesh", () => 
{
    describe("readVertices", () => 
    {
        test('simple vertex, axis test', () => {
            const testFile = new BinaryFile(Buffer.from([
            //      10-bits     |   11-bits   |     11-bits
            //         Z        |      Y      |        X
                0b00000000, 0b00000000, 0b00000111, 0b11111111,
                0b00000000, 0b00111111, 0b11111000, 0b00000000,
                0b11111111, 0b11000000, 0b00000000, 0b00000000,

                0b11111111, 0b11000000, 0b00000111, 0b11111111,
                0b00000000, 0b00000000, 0b00000000, 0b00000000,
                0b11111111, 0b11111111, 0b11111111, 0b11111111, 
            ]));

            expect(readVertices(testFile, 6)).toEqual([
                new Vector3([MAX_X,   0,     0  ]),
                new Vector3([  0,   MAX_Y,   0  ]),
                new Vector3([  0,     0,   MAX_Z]),
                new Vector3([MAX_X,   0,   MAX_Z]),
                new Vector3([  0,     0,     0  ]),
                new Vector3([MAX_X, MAX_Y, MAX_Z]),
            ]);
        });

        test('vertex with values', () => {
            const testFile = new BinaryFile(Buffer.from([
            //    ZZZZZZZZ    ZZYYYYYY    YYYYYXXX    XXXXXXXX     
                0b10000100, 0b01010100, 0b00010111, 0b11001011,
            ]));

            expect(readVertices(testFile, 1)).toEqual([
                new Vector3([
                    0b11111001011,
                    0b01010000010,
                    0b1000010001,
                ]),
            ]);
        });
    });

    describe("writeVertices", () => 
    {
        test('simple vertex, axis test', () => {
            const testFile = new BinaryFile();

            writeVertices(testFile, [
                [1.0,   0,   0],
                [  0, 1.0,   0],
                [  0,   0, 1.0],
                [1.0,   0, 1.0],
                [  0,   0,   0],
                [1.0, 1.0, 1.0],
            ], defaultOffset, defaultScale);

            expect(testFile.getBuffer()).toEqual(Buffer.from([
            //      10-bits     |   11-bits   |     11-bits
            //         Z        |      Y      |        X
                0b00000000, 0b00000000, 0b00000111, 0b11111111,
                0b00000000, 0b00111111, 0b11111000, 0b00000000,
                0b11111111, 0b11000000, 0b00000000, 0b00000000,

                0b11111111, 0b11000000, 0b00000111, 0b11111111,
                0b00000000, 0b00000000, 0b00000000, 0b00000000,
                0b11111111, 0b11111111, 0b11111111, 0b11111111, 
            ]));
        });

        test('vertex with values', () => {

            const testFile = new BinaryFile();

            writeVertices(testFile, [
                [0.9745969711773327, 0.3136297020029311, 0.5171065493646139]
            ], defaultOffset, defaultScale);

            expect(testFile.getBuffer()).toEqual(Buffer.from([
            //    ZZZZZZZZ    ZZYYYYYY    YYYYYXXX    XXXXXXXX     
                0b10000100, 0b01010100, 0b00010111, 0b11001011,
            ]));
        });

        test('example vertex with offset and scale', () => {

            const testFile = new BinaryFile();
            const scale = getVertexScale({min: [-2, 0, 2], max: [2, 3, 4]});
            const offset = [-2, 0, 2];

            writeVertices(testFile, [
                [-2.0, 1.0, 2.5],
                [2.0, 2.5, 3.8],
            ], offset, scale);

            expect(testFile.getBuffer()).toEqual(Buffer.from([
                0x40, 0x15, 0x50, 0x00,
                0xe6, 0x75, 0x57, 0xff
            ]));
        });
    });

    describe("readSharedVertices", () => 
    {
        test('simple vertex, axis test', () => {
            const testFile = new BinaryFile(Buffer.from([
            //                22-bits           |           21-bits             |          21-bits
            //                   Z              |              Y                |             X
                0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00011111, 0b11111111, 0b11111111,
                0b00000000, 0b00000000, 0b00000011, 0b11111111, 0b11111111, 0b11100000, 0b00000000, 0b00000000,
                0b11111111, 0b11111111, 0b11111100, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000,

                0b11111111, 0b11111111, 0b11111100, 0b00000000, 0b00000000, 0b00011111, 0b11111111, 0b11111111,
                0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000,
                0b11111111, 0b11111111, 0b11111111, 0b11111111, 0b11111111, 0b11111111, 0b11111111, 0b11111111, 
            ]));

            expect(readSharedVertices(testFile, 6, [0,0,0], [1,1,1])).toEqual([
                [1, 0, 0], [0, 1, 0], [0, 0, 1],
                [1, 0, 1], [0, 0, 0], [1, 1, 1],
            ]);
        });

        test('simple vertex with scale and offset', () => {
            const testFile = new BinaryFile(Buffer.from([
                0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
            ]));

            expect(readSharedVertices(testFile, 1, [0, 2.5, -3], [1.0, 2.0, 0.5])).toEqual([
                [1.0, 4.5, -2.5]
            ]);
        });

        test('vertex with values', () => {
            const testFile = new BinaryFile(Buffer.from([
            //    ZZZZZZZZ    ZZZZZZZZ    ZZZZZZYY    YYYYYYYY    YYYYYYYY    YYYXXXXX    XXXXXXXX    XXXXXXXX
                0b11000000, 0b00000000, 0b00000011, 0b11111111, 0b11111111, 0b11100001, 0b00100010, 0b01000000,
            //                 ~0.75            |             =1.0               |        ~0.035
            ]));

            expect(readSharedVertices(testFile, 1, [0,0,0], [1,1,1])).toMatchCloseTo([
                [0.035, 1.0, 0.75]
            ], 3);
        });
    });

    
    describe("writeSharedVertices", () => 
    {
        test('simple vertex, axis test', () => {
            const testFile = new BinaryFile();

            writeSharedVertices(testFile, [
                [1, 0, 0], [0, 1, 0], [0, 0, 1],
                [1, 0, 1], [0, 0, 0], [1, 1, 1],
            ]);

            expect(testFile.getBuffer()).toEqual(Buffer.from([
            //                22-bits           |           21-bits             |          21-bits
            //                   Z              |              Y                |             X
                0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00011111, 0b11111111, 0b11111111,
                0b00000000, 0b00000000, 0b00000011, 0b11111111, 0b11111111, 0b11100000, 0b00000000, 0b00000000,
                0b11111111, 0b11111111, 0b11111100, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000,

                0b11111111, 0b11111111, 0b11111100, 0b00000000, 0b00000000, 0b00011111, 0b11111111, 0b11111111,
                0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000,
                0b11111111, 0b11111111, 0b11111111, 0b11111111, 0b11111111, 0b11111111, 0b11111111, 0b11111111, 
            ]));
        });

        test('vertex with values', () => {
            const testFile = new BinaryFile();

            writeSharedVertices(testFile, [
                [0.035430925097, 1.0, 0.7500001]
            ]);

            expect(testFile.getBuffer()).toEqual(Buffer.from([
            //    ZZZZZZZZ    ZZZZZZZZ    ZZZZZZYY    YYYYYYYY    YYYYYYYY    YYYXXXXX    XXXXXXXX    XXXXXXXX
                0b11000000, 0b00000000, 0b00000011, 0b11111111, 0b11111111, 0b11100001, 0b00100010, 0b01000000,
            ]));
        });
    });

    describe('getVertexScale', () => {

        test('zero size', () => {
            const scale = getVertexScale({
                min: [0,0,0],
                max: [0,0,0],
            });
            expect(scale).toEqual([0, 0, 0]);
        });

        test('unit domain', () => {
            const scale = getVertexScale({
                min: [0, 0, 0],
                max: [2047, 2047, 1023],
            });
            expect(scale).toEqual([1, 1, 1]);
        });

        test('example domain', () => {
            const scale = getVertexScale({
                min: [-10, 0, -30],
                max: [ 20, 15,  0],
            });
            expect(scale).toMatchCloseTo([
                0.014655,
                0.007327,
                0.029325,
            ], 6);
        });
    });
});
