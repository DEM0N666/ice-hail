/**
* @copyright 2019 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

const DataPointer = require('../../../src/dataPointer');
const {readMaterials, writeMaterials} = require('../../../src/classes/compressedMesh/material');
const BinaryFile = require('../../../src/binaryFile');

describe("compressedMesh", () => 
{
    describe("readMaterials", () => 
    {
        test('single material', () => {
            const testfile = new BinaryFile(Buffer.from([
                0,0,0,0,
                ...(['T', 'E', 'S', 'T'].map(c => c.charCodeAt(c)))
            ]));

            const materials = readMaterials(testfile, 1);
            expect(materials).toEqual(["TEST"]);
        });

        test('multiple material', () => {
            const testfile = new BinaryFile(Buffer.from([
                0,0,0,0,  0,0,0,0,  0,0,0,0, // starts with 4-zeros per material
                ...(['T', 'E', 'S', 'T'].map(c => c.charCodeAt(c))), 0, 0,
                ...(['A', 'b', 'c'].map(c => c.charCodeAt(c))), 0,
                ...(['L', 'o', 'n', 'g', 'e', 'r',',', 'N', 'a', 'm', 'e'].map(c => c.charCodeAt(c)))
            ]));

            const materials = readMaterials(testfile, 3);
            expect(materials).toEqual([
                "TEST", "Abc", "Longer,Name"
            ]);
        });
    });

    describe("writeMaterials", () => 
    {
        test('single material', () => {
            const testFile = new BinaryFile();
            const ptrMaterial = new DataPointer(testFile);
            writeMaterials(testFile, ["TEST"], ptrMaterial);

            expect(testFile.getBuffer()).toEqual(Buffer.from([
                0,0,0,0,
                ...(['T', 'E', 'S', 'T'].map(c => c.charCodeAt(c))), 
                0, 0 // after the NIL marker, it's aligned to 2-bytes
            ]));
        });

        test('single material', () => {
            const testFile = new BinaryFile();
            const ptrMaterial = new DataPointer(testFile);
            writeMaterials(testFile, [
                "TEST", "Abc", "Longer,Name"
            ], ptrMaterial);

            expect(testFile.getBuffer()).toEqual(Buffer.from([
                0,0,0,0,  0,0,0,0,  0,0,0,0,
                ...(['T', 'E', 'S', 'T'].map(c => c.charCodeAt(c))), 0, 0,
                ...(['A', 'b', 'c'].map(c => c.charCodeAt(c))), 0,
                ...(['L', 'o', 'n', 'g', 'e', 'r',',', 'N', 'a', 'm', 'e'].map(c => c.charCodeAt(c))), 0
            ]));
        });
    });
});