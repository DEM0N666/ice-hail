/**
* @copyright 2019 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

const hkpConvexTranslateShape = require("../../src/classes/hkpConvexTranslateShape");
const BinaryFile = require("../../src/binaryFile");

describe(hkpConvexTranslateShape.name, () => 
{
    test('pointer', () => {
        const file = new BinaryFile();
        const entry = new hkpConvexTranslateShape(file, {
            translate: [3.0, 4.0, 5.0],
        });
        entry._create();

        expect(entry.pointer.length).toEqual(0);
    });

    test('integration test, no radius', () => {
        const file = new BinaryFile();
        const entry = new hkpConvexTranslateShape(file, {
            translate: [3.0, 4.0, 5.0],
        });
        entry._create();

        expect(file.getBuffer()).toEqualBinary(
`00000000: 0000 0000 0000 0000 0004 0000 1000 8002  ................
00000010: 3d4c cccd 0000 0000 0000 0000 0000 0000  =LÌÍ............
00000020: 4040 0000 4080 0000 40a0 0000 0000 0000  @@..@...@.......`
        );
    });

    test('integration test, radius set', () => {
        const file = new BinaryFile();
        const entry = new hkpConvexTranslateShape(file, {
            radius: 3.0,
            translate: [3.0, 4.0, 5.0],
        });
        entry._create();

        expect(file.getBuffer()).toEqualBinary(
`00000000: 0000 0000 0000 0000 0004 0000 1000 8002  ................
00000010: 4040 0000 0000 0000 0000 0000 0000 0000  @@..............
00000020: 4040 0000 4080 0000 40a0 0000 0000 0000  @@..@...@.......`
        );
    });

    test('integration test, no transform set', () => {
        const file = new BinaryFile();
        const entry = new hkpConvexTranslateShape(file, {});
        entry._create();

        expect(file.getBuffer()).toEqualBinary(
`00000000: 0000 0000 0000 0000 0004 0000 1000 8002  ................
00000010: 3d4c cccd 0000 0000 0000 0000 0000 0000  =LÌÍ............
00000020: 0000 0000 0000 0000 0000 0000 0000 0000  ................`
        );
    });
});