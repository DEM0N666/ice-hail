/**
* @copyright 2019 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

let {readBvCompoundTree, _bvCompoundTreeFromArray} = require('../../../src/classes/compoundShape/bvCompoundTreeReader');
const BinaryFile = require('../../../src/binaryFile');
const BvTree = require('../../../src/classes/bvTree/tree');
const {Domain, DomainEntry} = require('../../../src/math/domain');

describe("compoundShape", () => 
{
    describe("bvTree._bvCompoundTreeFromArray", () => 
    {
        test('emtpy tree', () => {

            const testDomain = {
                min: [-1.0, -1.0, -1.0],
                max: [ 1.0,  1.0,  1.0]
            };

            expect(() => _bvCompoundTreeFromArray([], testDomain))
                .toThrow("Compressed Mesh contains an empty BvCompoundTree");
        });

        test('single entry', () => {
            const testDomain = {
                min: [-1.0, -1.0, -1.0],
                max: [ 1.0,  1.0,  1.0]
            };

            const tree = _bvCompoundTreeFromArray([
                {index: 0, data: [0xFF, 0xFF, 0xFF]}
            ], testDomain);
            
            expect(tree.index).toEqual(0);
            expect(tree.children).toBeUndefined();
        });

        test('simple tree', () => {
            const testDomain = {
                min: [-1.0, -1.0, -1.0],
                max: [ 1.0,  1.0,  1.0]
            };

            const tree = _bvCompoundTreeFromArray([
                {index: 1, flags: 0x80, data: [0xFF, 0xFF, 0xFF]}, // points to index 2
                    {index: 1, flags: 0, data: [0xFF, 0xFF, 0xFF]}, // child-0
                    {index: 3, flags: 0, data: [0xFF, 0xFF, 0xFF]}  // child-1
            ], testDomain);
            
            expect(tree.index).toBeUndefined();
            expect(tree.childA).toBeDefined();
            expect(tree.childB).toBeDefined();

            expect(tree.childA.index).toEqual(1);
            expect(tree.childA.children).toBeUndefined();

            expect(tree.childB.index).toEqual(3);
            expect(tree.childB.children).toBeUndefined();
        });

        test('nested tree', () => {
            const testDomain = {
                min: [-1.0, -1.0, -1.0],
                max: [ 1.0,  1.0,  1.0]
            };

            const tree = _bvCompoundTreeFromArray([
                {index: 2, flags: 0x80, data: [0x00, 0x00, 0x00]},
                    {index: 1, flags: 0x80, data: [0x00, 0x00, 0x00]}, // child-0
                        {index: 1, flags: 0, data: [0x00, 0x00, 0x00]},  // child-0-0
                        {index: 3, flags: 0, data: [0x00, 0x00, 0x00]},  // child-0-1
                    {index: 2, flags: 0, data: [0x00, 0x00, 0x00]},  // child-1
            ], testDomain);
            
            expect(tree).toEqual(
                new BvTree(testDomain,
                    new BvTree(testDomain,
                        new DomainEntry(testDomain, 1),
                        new DomainEntry(testDomain, 3)
                    ),
                    new DomainEntry(testDomain, 2)
                )
            );
        });
    }); 
    
    describe("bvTree.readBvTree", () => 
    {    
        test('simple tree', () => {
            const testDomain = {
                min: [-1.0, -1.0, -1.0],
                max: [ 1.0,  1.0,  1.0]
            };
            const testFile = new BinaryFile(Buffer.from([
                0xFF, 0xFF, 0xFF, 0x80, 0,1,
                0xFF, 0xFF, 0xFF, 0x00, 0,1,
                0xFF, 0xFF, 0xFF, 0x00, 0,6,
            ]));

            const tree = readBvCompoundTree(testFile, 3, testDomain);
            
            expect(tree.index).toBeUndefined();
            expect(tree.childA).toBeDefined();
            expect(tree.childB).toBeDefined();

            expect(tree.childA.index).toEqual(1);
            expect(tree.childA.children).toBeUndefined();

            expect(tree.childB.index).toEqual(6);
            expect(tree.childB.children).toBeUndefined();
        });
    });
});