const HKRBHelper = require("./../src/hkrbHelper");

describe('HKRB-Helper', () => 
{
    describe('writeInverseMatrix3x4', () => 
    {
        test("shold invert a simple matrix", () => {

            const result = [];
            const file = {
                write: (_, val) => result.push(val)
            };

            HKRBHelper.writeInverseMatrix3x4(file, [
                [1.0, 2.0, 3.0],
                [1.1, 2.1, 3.1],
                [1.2, 2.2, 3.2],
                [1.3, 2.3, 3.3],
            ]);

            expect(result).toEqual([
                1.0, 1.1, 1.2, 1.3,
                2.0, 2.1, 2.2, 2.3,
                3.0, 3.1, 3.2, 3.3,
            ]);
        }); 
    });
});