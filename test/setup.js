/**
* @copyright 2019 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

const diff = require('jest-diff');
const dump = require('buffer-hexdump');
const fs = require('fs-extra');
const path = require('path');

// add custom matcher functions

const {toBeDeepCloseTo,toMatchCloseTo} = require('jest-matcher-deep-close-to');
expect.extend({toBeDeepCloseTo, toMatchCloseTo});

expect.extend({
    toEqualBinary(recieved, expected)
    {
        const receivedHex = dump(recieved);
        const expectedHex = expected instanceof Buffer ? dump(expected) : expected;
        
        //console.log(receivedHex);

        return receivedHex.localeCompare(expectedHex) == 0 ?
        {
            message: () => this.utils.matcherHint("toEqual", receivedHex, expectedHex),
            pass: true,
        } :
        {
            message: () => {
                const difference = diff(expectedHex, receivedHex, {
                    expand: this.expand,
                });
                return difference;
            },
            pass: false,
        };
    }
});

// delete old integration test files (ignored in gitignore)

fs.emptyDirSync(path.join('test', 'integration', 'tmp'));