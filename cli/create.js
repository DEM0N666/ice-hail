#!/usr/bin/env node
/**
* @copyright 2019 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

const packageJson = require('../package.json');
const fs = require('fs-extra');
const program = require('commander');

const generateFile = require("../src/generateFile");

program.version(packageJson.version, '-v, --version')
    .arguments('<jsonFile> [outputFile]', 'Creates a binary HK file from a JSON file')
    .action((jsonFile, outputFile) => create(jsonFile, outputFile).catch((e) => {
        console.error(e);
        process.exit(1);
    }))
    .parse(process.argv);

if (process.argv.length < 3) {
    program.help();
}

async function create(jsonFile, outputFile)
{
    console.log(`Loading JSON file '${jsonFile}'...`);
    const collData = await fs.readJSON(jsonFile);        
    
    if(!outputFile) {
        const extension = collData.compound ? ".hksc" : ".hkrb";
        outputFile =  jsonFile.split(".").slice(0,-1).join(".") + extension;
    }

    console.log(`Creating HK-File file...`);
    try{
        await fs.writeFile(outputFile, generateFile(collData));
    } catch(e) {
        console.log(e);
    }

    console.log(`File saved to '${outputFile}'`);
}