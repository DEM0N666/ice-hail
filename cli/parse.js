#!/usr/bin/env node
/**
* @copyright 2019 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

const packageJson = require('../package.json');
const fs = require('fs-extra');
const path = require("path");
const program = require('commander');

const parseFile = require("../src/parseFile");

program.version(packageJson.version, '-v, --version')
    .arguments('<hkFile> [outputFile]', 'Parses a binary HK file into a readable JSON file')
    .option('-p, --pretty', 'Beautify the created JSON file', program.BOOL, false)
    .action((hkFile, outputFile, options) => parse(hkFile, outputFile, options).catch((e) => {
        console.error(e);
        process.exit(1);
    }))
    .parse(process.argv);

if (process.argv.length < 3) {
    program.help();
}

async function parse(hkFile, outputFile, options)
{
    const hkData = await fs.readFile(hkFile);
    outputFile = outputFile || hkFile.split(".").slice(0,-1).join(".") + ".json";
    
    console.log("Parsing HK-File...");
    const json = parseFile(hkData, path.extname(hkFile));

    console.log("Writing Output-File...");            
    fs.writeJson(outputFile, json, options.pretty ? {spaces: 2} : {});

    console.log(`File saved to '${outputFile}'`);
};